# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 18:49:40 2021

@author: Guillaume
"""
import sciann as sn
import numpy as np
import scipy.io
import matplotlib.pyplot as plt

''' Navier-Stokes equations '''

# =============================================================================
# Affichage courbe loss
# =============================================================================

def PrepareData(num_data=5000, random=True):
    data = scipy.io.loadmat('cylinder_nektar_wake.mat')
    
    U_star = data['U_star'] # N x 2 x T
    P_star = data['p_star'] # N x T
    t_star = data['t'] # T x 1
    X_star = data['X_star'] # N x 2
    
    N = X_star.shape[0]
    T = t_star.shape[0]
    
    # Rearrange Data 
    XX = np.tile(X_star[:,0:1], (1,T)) # N x T
    YY = np.tile(X_star[:,1:2], (1,T)) # N x T
    TT = np.tile(t_star, (1,N)).T # N x T
    
    UU = U_star[:,0,:] # N x T
    VV = U_star[:,1,:] # N x T
    PP = P_star # N x T
    
    # Pick random data.
    if random:
        idx = np.random.choice(N*T, num_data, replace=False)
    else:
        idx = np.arange(0, N*T)
    
    x = XX.flatten()[idx,None] # NT x 1
    y = YY.flatten()[idx,None] # NT x 1
    t = TT.flatten()[idx,None] # NT x 1
    
    u = UU.flatten()[idx,None] # NT x 1
    v = VV.flatten()[idx,None] # NT x 1
    p = PP.flatten()[idx,None] # NT x 1
 
    return (x,y,t,u,v,p)
    
#     data = scipy.io.loadmat('cylinder_nektar_wake.mat')
    
#     U_star = data['U_star'] # N x 2 x T
#     P_star = data['p_star'] # N x T
#     t_star = data['t'] # T x 1
#     X_star = data['X_star'] # N x 2
    
#     N = X_star.shape[0]
#     T = t_star.shape[0]
    
#     # Rearrange Data 
#     XX = np.tile(X_star[:,0:1], (1,T)) # N x T
#     YY = np.tile(X_star[:,1:2], (1,T)) # N x T
#     TT = np.tile(t_star, (1,N)).T # N x T
    
#     UU = U_star[:,0,:] # N x T
#     VV = U_star[:,1,:] # N x T
#     PP = P_star # N x T
    
#     # Pick random data.
#     if random:
#         idx = np.random.choice(N*T, num_data, replace=False)
#     else:
#         idx = np.arange(0, N*T)
    
#     x = XX.flatten()[idx,None] # NT x 1
#     y = YY.flatten()[idx,None] # NT x 1
#     t = TT.flatten()[idx,None] # NT x 1
    
#     u = UU.flatten()[idx,None] # NT x 1
#     v = VV.flatten()[idx,None] # NT x 1
#     p = PP.flatten()[idx,None] # NT x 1
 
#     return (x,y,t,u,v,p)

# x_data, y_data, t_data, u_data, v_data, p_data = PrepareData(5000, random=True)
x_train, y_train, t_train, u_train, v_train, p_train = PrepareData(5000, random=True)

#création des variables
x = sn.Variable("x", dtype = 'float64')
y = sn.Variable("y", dtype = 'float64')
t = sn.Variable("t", dtype = 'float64')

p = sn.Field("p")
psi = sn.Field("psi")

p = sn.Functional("p", [t,x,y], 8*[20], 'tanh')
psi = sn.Functional("psi", [t,x,y], 8*[20], 'tanh')

# lamb1 = sn.Parameter(0.0, inputs=[x,y], name="lamb1")
# lamb2 = sn.Parameter(0.0, inputs=[x,y], name="lamb2")
lamb1 = sn.Parameter(np.random.rand(), inputs=[x,y,t], name="lamb1")
lamb2 = sn.Parameter(np.random.rand(), inputs=[x,y,t], name="lamb2")
# lamb1 = 0.9967
# lamb2 = 0.0110

#définition des dérivés
u,v = sn.diff(psi,y), -sn.diff(psi,x)
u_t, v_t = sn.diff(u,t), sn.diff(v,t)
u_x, u_y = sn.diff(u,x), sn.diff(u,y)
v_x, v_y = sn.diff(v,x), sn.diff(v,y)
u_xx, u_yy = sn.diff(u,x,order=2), sn.diff(u,y,order=2)
v_xx, v_yy = sn.diff(v,x,order=2), sn.diff(v,y,order=2)
p_x,p_y = sn.diff(p,x), sn.diff(p,y)

d1 = sn.Data(u)
d2 = sn.Data(v)
d3 = sn.Data(p)

c1 = sn.Tie(-p_x, u_t+lamb1*(u*u_x+v*u_y)-lamb2*(u_xx+u_yy))
c2 = sn.Tie(-p_y, v_t+lamb1*(u*v_x+v*v_y)-lamb2*(v_xx+v_yy))
c3 = sn.Data(u_x + v_y)
c4 = psi*0.0

model = sn.SciModel(
    inputs=[x, y, t],
    targets=[d1, d2,d3, c1, c2, c3, c4],
    loss_func="mse",
)

input_data = [x_train, y_train, t_train]

data_d1 = u_train
data_d2 = v_train
data_d3 = p_train
data_c1 = 'zeros'
data_c2 = 'zeros'
data_c3 = 'zeros'
data_c4 = 'zeros'

target_data = [data_d1, data_d2, data_d3, data_c1, data_c2, data_c3, data_c4]

history = model.train(
    x_true=input_data,
    y_true=target_data,
    epochs=5000,
    batch_size=100,
    shuffle=True,
    learning_rate=0.001,
    reduce_lr_after=100,
    stop_loss_value=1e-8,
    verbose=1
)

print("lambda1: {},  lambda2: {}".format(lamb1.value, lamb2.value))

plt.semilogy(history.history['loss'])
plt.xlabel('epochs')
plt.ylabel('loss')


# =============================================================================
# Affichage modélisation par RN
# =============================================================================

# #création des variables
# x = sn.Variable("x", dtype = 'float64')
# y = sn.Variable("y", dtype = 'float64')
# t = sn.Variable("t", dtype = 'float64')

# p = sn.Field("p")
# psi = sn.Field("psi")

# p = sn.Functional("p", [t,x,y], 8*[20], 'tanh')
# psi = sn.Functional("psi", [t,x,y], 8*[20], 'tanh')

# lamb1 = sn.Parameter(0.0, inputs=[x,y], name="lamb1")
# lamb2 = sn.Parameter(0.0, inputs=[x,y], name="lamb2")
# # lamb1 = 0.9967
# # lamb2 = 0.0110

# x_data, y_data, t_data = np.meshgrid(np.linspace(-1, 1, 100), np.linspace(0, 1, 100),np.linspace(0, 1, 100))

# #définition des dérivés
# u,v = sn.diff(psi,y), -sn.diff(psi,x)
# u_t, v_t = sn.diff(u,t), sn.diff(v,t)
# u_x, u_y = sn.diff(u,x), sn.diff(u,y)
# v_x, v_y = sn.diff(v,x), sn.diff(v,y)
# u_xx, u_yy = sn.diff(u,x,order=2), sn.diff(u,y,order=2)
# v_xx, v_yy = sn.diff(v,x,order=2), sn.diff(v,y,order=2)
# p_x,p_y = sn.diff(p,x), sn.diff(p,y)

# L1 = u_t + p_x + lamb1*(u*u_x + v*u_y) - lamb2*(u_xx + u_yy)
# L2 = v_t + p_y + lamb1*(u*v_x + v*v_y) - lamb2*(v_xx + v_yy)
# L3 = u
# L4 = v

# #optimisation
# m = sn.SciModel([t,x,y], [L1,L2,L3,L4], "mse", "Adam")
# m.train([t_data, x_data, y_data], ['zeros','zeros', u_train, v_train],batch_size = 64, epochs = 100)

# p_test = p.eval(m, [t_data, x_data,y_data])
# psi_test = psi.eval(m, [t_data, x_data,y_data])

# # fig1 = plt.figure(1,figsize=(3, 4))
# # plt.pcolormesh(x_data, y_data, t_data, p_test, cmap='seismic')
# # plt.xlabel('x')
# # plt.ylabel('t')
# # plt.colorbar()

# # fig2 = plt.figure(2,figsize=(3, 4))
# # plt.pcolormesh(x_data, y_data, t_data, psi_test, cmap='seismic')
# # plt.xlabel('x')
# # plt.ylabel('t')
# # plt.colorbar()
