# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 13:30:58 2021

@author: Guillaume
"""

import sciann as sn
import numpy as np
from sciann.utils.math import pi
import matplotlib.pyplot as plt

plt.ion()

''' Initialisation du RN '''

from DataGenerator import DataGeneratorXY

#génération des data#
dg = DataGeneratorXY(
        X = [-pi, pi],
        Y = [-pi, pi],
        targets = ['domain', 'bc-left', 'bc-right', 'bc-bot', 'bc-top'], 
        num_sample = 1000
    )
dg.plot_data()
dg.plot_sample_batch(100)

x_data = dg.input_data[0]
y_data = dg.input_data[1]

#solution exacte#
def f1(x,y):
    f1 = np.sin(x)*np.sin(y)
    return f1

f_data = f1(x_data,y_data)

#création des variables#
x = sn.Variable("x")
y = sn.Variable("y")
f = sn.Field("f")

#définition du RN#
f = sn.Functional("f",[x,y],[6,6,6],"tanh")

#Les poids des neurones sont randomisés
#on peut les récupérer avec
# f.get_weights()
#on peut les paramétrer avec
# f.set_weights()


''' Problème d'Optimisation '''

#définition du modèle
m = sn.SciModel(
    inputs = [x,y],
    targets = [f],
    loss_func = "mse", 
    optimizer = "adam"
    )

#training du modèle
m.train([x_data,y_data], [f_data], epochs = 400)

#evaluation des 
f_test = f.eval(m, [x_data,y_data])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_data, y_data, f1(x_data,y_data))
ax.set_title('solution exacte')

fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')
ax2.scatter(x_data, y_data, f.eval(m,[x_data, y_data]))
ax2.set_title('solution évaluée')

z = f1(x_data,y_data) - f.eval(m,[x_data, y_data])

fig6 = plt.figure()
ax6 = fig6.add_subplot(111, projection='3d')
ax6.scatter(x_data, y_data, abs(z))
ax6.set_title('erreur absolue: |f_eval - f_vraie|')
plt.show()

