# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 16:31:15 2021

@author: Guillaume
"""

import numpy as np
import matplotlib.pyplot as plt 
import sciann as sn


# =============================================================================
# Affichage solution exacte
# =============================================================================
# x_data, y_data, vol_data = np.meshgrid(
#     np.linspace(-1, 1, 200), 
#     np.linspace(-1, 1, 200),
#     np.linspace(-1, 1, 200)
# )

# f_data = (0.1*np.sin(2*np.pi*x_data)+np.tanh(10*x_data))*np.sin(2*np.pi*y_data)

# def f(x,y):
#     f = np.sin(2*np.pi)*(20*np.tanh(10*x)*(10*np.tanh(10*x)**2-10) - (2*np.pi**2*np.sin(2*np.pi*x))/5) -4*np.pi**2*np.sin(2*np.pi*y)*(np.tanh(10*x)+(np.sin(2*np.pi*x))/10)
#     return f

# fxy_data = f(x_data,y_data)

# plt.pcolormesh(x_data, y_data, fxy_data, cmap='plasma')
# plt.xlabel("x")
# plt.ylabel("y")
# plt.colorbar()

from DataGenerator import DataGeneratorXY

dg = DataGeneratorXY(
        X = [-1, 1],
        Y = [-1, 1],
        targets = ['domain', 'bc-left', 'bc-right', 'bc-bot', 'bc-top'], 
        num_sample = 1000
    )
# dg.plot_data()
# dg.plot_sample_batch(100)

x_data = dg.input_data[0]
y_data = dg.input_data[1]

Q_data = np.zeros(x_data.shape)
Q_data[dg.target_data[0][0]] = np.random.rand(Q_data[dg.target_data[0][0]].shape[0], 1)

x = sn.Variable('x')
y = sn.Variable('y')

Q = sn.Functional('Q',[x,y],4*[20], 'sigmoid')
m = sn.SciModel([x,y],[Q])
m.train([x_data, y_data],[Q_data])
Q.set_trainable(False)

T = sn.Functional('T', [x,y], 4*[20], 'tanh')
Q_x, Q_y = sn.diff(Q,x), sn.diff(Q,y)
T_x, T_y = sn.diff(T,x), sn.diff(T,y)
fxy = sn.Variable('fxy')
vol = sn.Variable('vol')
J = (Q_x*T_x + Q_y*T_y + Q*fxy)*vol

m = sn.SciModel([x,y,vol],[J,T], "mse")

'''Il manque la définition des variables vol, bc_ids, bc_vals'''

# m.train(
#         [x_data, y_data, vol_data,fxy_data],
#         ['zeros', (bc_ids,bc_vals)]
#         )

fxy_test = fxy.eval(m, [x_data,y_data])
Q_test = Q.eval(m, [x_data,y_data])

plt.pcolormesh(x_data, y_data, Q_test, cmap='plasma')
plt.xlabel("x")
plt.ylabel("y")
plt.colorbar()
